#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdbool.h>
#define PORT 8080
#define BUFFER_SIZE 200

char priviledge[BUFFER_SIZE] = {0};
bool input_wait = 0;

int createSocket();
void login(int socket, int argc, char *argv[]);
void *inputHandler(void *client_socket);
void *outputHandler(void *client_socket);

int main(int argc, char *argv[]) {
	pthread_t tid[2];
	int client_socket = createSocket();
	
	login(client_socket, argc, argv);
	
	pthread_create(&tid[0], NULL, &inputHandler, (void*)&client_socket);
	pthread_create(&tid[1], NULL, &outputHandler, (void*)&client_socket);
	
	pthread_join(tid[0], NULL);
	pthread_join(tid[1], NULL);
	
	close(client_socket);
}

int createSocket() {
	struct sockaddr_in address;
    	int sock = 0, valread;
    	struct sockaddr_in serv_addr;
    	
    	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        	printf("\n Socket creation error \n");
        	exit(EXIT_FAILURE);
    	}
    	
    	memset(&serv_addr, '0', sizeof(serv_addr));
  
    	serv_addr.sin_family = AF_INET;
    	serv_addr.sin_port = htons(PORT);
    	
    	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        	printf("\nInvalid address/ Address not supported \n");
        	exit(EXIT_FAILURE);
    	}
    	
    	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        	printf("\nConnection Failed \n");
        	exit(EXIT_FAILURE);
    	}	
    	return sock;
}

void login(int socket, int argc, char *argv[]) {
	char buf[BUFFER_SIZE];
	if(getuid() == 0) {
		send(socket, "LOGIN root root", BUFFER_SIZE, 0);
		printf("LOGIN root root\n");
		strcpy(priviledge, "root");
	}
	else if(argc == 5 && strcmp(argv[1] , "-u") == 0
       	&& strcmp(argv[3] , "-p") == 0) {
       	sprintf(buf, "LOGIN %s %s", argv[2], argv[4]);
       	send(socket, buf, BUFFER_SIZE, 0);
       	printf("%s\n", buf);
       	strcpy(priviledge, "user");
       }
       else {
       	printf("ERROR: invalid argument\n");
       	exit(EXIT_FAILURE);
       }
       read(socket, buf, BUFFER_SIZE);
       printf("%s\n", buf);
       
       if(strcmp(buf, "Login successfully") != 0) {
       	printf("ERROR: wrong username or password\n");
       	exit(EXIT_FAILURE);
       }
}

void *inputHandler(void *client_socket) {
	int socket = *(int *) client_socket;
	char message[BUFFER_SIZE];
	while(true) {
		while(input_wait) continue;
		
		printf("Enter your input: ");
		scanf(" %[^\n]", message);
		if(strcmp(message, "EXIT") == 0) {
			printf("Exit successfully\n");
			exit(EXIT_SUCCESS);
		}
		strcat(message, " ");
		strcat(message, priviledge);
		send(socket, message, BUFFER_SIZE, 0);
		
		input_wait = true;
	}
}

void *outputHandler(void *client_socket) {
	int socket = *(int *) client_socket;
	char message[BUFFER_SIZE];
	while(true) {
		while(!input_wait) continue;
		
		read(socket, message, BUFFER_SIZE);
		printf("%s\n", message);
		
		input_wait = false;
	}
}
