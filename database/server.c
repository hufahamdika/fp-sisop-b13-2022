#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <limits.h>
#include <dirent.h>
#define PORT 8080
//#define SIZE 1024

int curr_fd = -1, curr_id = -1;
const int SIZE_BUFFER = sizeof(char) * 1000; 
void *routes(void *argv);
void logging(char *string);
bool login(int fd, char *username, char *password);
bool fileExists(char *filename);

char workDir[PATH_MAX];
int main() {
    pid_t pid, sid;
    pid = fork();

    if (pid < 0)
    exit(EXIT_FAILURE);

    if (pid > 0)
    exit(EXIT_SUCCESS);

    umask(0);

    sid = setsid();
    if (sid < 0)
    exit(EXIT_FAILURE);

    getcwd(workDir, sizeof(workDir));

    if ((chdir(workDir)) < 0)
    exit(EXIT_FAILURE);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        struct sockaddr_in address, new_addr;
        int fd, new_fd, ret_val, opt = 1;
        socklen_t addrlen;
        pthread_t tid;
        char buf[1000];

        fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (fd == -1) {
            fprintf(stderr, "socket failed!! [%s]\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
            perror("setsockopt");
            exit(EXIT_FAILURE);
        }

        address.sin_family = AF_INET;
        address.sin_port = htons(PORT);
        address.sin_addr.s_addr = INADDR_ANY;

        ret_val = bind(fd, (struct sockaddr *)&address, sizeof(struct sockaddr_in));
        if (ret_val != 0) {
            fprintf(stderr, "bind failed!");
            close(fd);
            exit(EXIT_FAILURE);
        }

        ret_val = listen(fd, 5);
        if (ret_val != 0) {
            fprintf(stderr, "listen failed");
            close(fd);
            exit(EXIT_FAILURE);
        }

        while (1) {
            new_fd = accept(fd, (struct sockaddr *)&new_addr, &addrlen);
            if (new_fd >= 0) {
                pthread_create(&tid, NULL, &routes, (void*)&new_fd);
            }
        }
    }
    return 0;
}

void *routes(void *argv) {
    int fd = *(int *)argv;
    char query[1000], buf[1000];
    char path[1000];

    while (read(fd, query, 1000) != 0) {
        puts(query);
        strcpy(buf, query);
        logging(buf);
        char *cmd = strtok(buf, " ");

        if (strcmp(cmd, "LOGIN") == 0) {
            char *username = strtok(NULL, " ");
            char *password = "root";
            if (!login(fd, username, password))
            break;
        }
        else if (strcmp(cmd, "CREATE") == 0) {
            cmd = strtok(NULL, " ");

            if (strcmp(cmd, "DATABASE") == 0) {
                char dbDir[5000], rootDir[5000], cwd[PATH_MAX], tempDir[PATH_MAX];
                char *token = strtok(NULL, " ");

                getcwd(cwd, sizeof(cwd));
                strcpy(tempDir, cwd);

                printf("tempDir: %s\n", tempDir);

                //
                if (strstr(cwd, "database/") != NULL) {
                    chdir("../../");
                }

                if (getcwd(cwd, sizeof(cwd)) != NULL) {
                    char *dbName = strtok(token, ";");

                    sprintf(dbDir, "%s/database/%s", cwd, dbName);
                    sprintf(rootDir, "%s/database", cwd);

                    DIR *dir = opendir(rootDir);
                    if (ENOENT == errno) {
                        mkdir(rootDir, 0777);
                    }

                    int check = mkdir(dbDir, 0777);
                    if (!check) {
                        write(fd, "Create DB Success\n", SIZE_BUFFER);

                        // DIKEMBALIKAN KE WORKDIR SEMULA
                        chdir(tempDir);
                    }
                    else {
                        write(fd, "Create Database is Failed\n", SIZE_BUFFER);
                    }
                }
            }
            else if (strcmp(cmd, "TABLE") == 0) {
                char fileDir[5000], cwd[PATH_MAX];
                char *tableName = strtok(NULL,  " ");
                printf("table: %s\n", tableName);

                if (getcwd(cwd, sizeof(cwd)) != NULL) {
                    FILE *data;
                    char *columnDetail = strtok(NULL, "()");

                    printf("column: %s\n", columnDetail);
                    sprintf(fileDir, "%s/%s", cwd, tableName);

                    printf("dir: %s\n", fileDir);

                    if (strstr(cwd, "database/") == NULL) {
                        write(fd, "Anda belum menggunakan database!\n", SIZE_BUFFER);
                    }
                    else if (fileExists(fileDir)) {
                        write(fd, "Tabel sudah dibuat sebelumnya!\n", SIZE_BUFFER);
                    }
                    else if (columnDetail == NULL) {
                        write(fd, "Invalid syntax!\n", SIZE_BUFFER);
                    }
                    else {
                        data = fopen(fileDir, "a");

                        fprintf(data, "%s\n", columnDetail);
                        fclose(data);
                        write(fd, "Tabel berhasil dibuat\n", SIZE_BUFFER);
                    }
                }
            }
            else {
                write(fd, "Invalid query on CREATE command\n", SIZE_BUFFER);
            }
        }
        else if (strcmp(cmd, "USE") == 0) {
            char cwd[PATH_MAX], dbDir[5000], *checkDir;
            cmd = strtok(NULL, " ");
            getcwd(cwd, sizeof(cwd));

            checkDir = strstr(cwd, "database");
            if (checkDir != NULL) {
                chdir("../../");;
            }

            if (getcwd(cwd, sizeof(cwd)) != NULL) {
                char temp[5000];
                char *dbName = strtok(cmd, ";");

                printf("%s\n", dbName);
                printf("cwd:%s\ncmd:%s\n", cwd, cmd);
                sprintf(dbDir, "%s/database/%s", cwd, cmd);
                printf("dir: %s\n", dbDir);

                int status = chdir(dbDir);
                printf("status: %d\n", status);

                if (status == 0) {
                    write(fd, "Pindah Database berhasil!\n", SIZE_BUFFER);
                }
                else {
                    write(fd, "Pindah Database gagal!\n", SIZE_BUFFER);
                }
            }
        }
        else if (strcmp(cmd, "DROP") == 0) {
            cmd = strtok(NULL, " ");

            if (strcmp(cmd, "DATABASE") == 0) {
                char string[5000], cwd[PATH_MAX], tempDir[PATH_MAX], *checkDir;
                char *token = strtok(NULL, " ");

                getcwd(cwd, sizeof(cwd));
                strcpy(tempDir, cwd);

                printf("tempDir: %s\n", tempDir);

                //KEMBALI KE PARENT KALAU LAGI USE
                checkDir = strstr(cwd, "database/");
                if (checkDir != NULL) {
                    chdir("../../");
                }

                if (getcwd(cwd, sizeof(cwd)) != NULL) {
                    DIR *dp;
                    struct dirent *ep;
                    char path[100], fileDir[5000];
                    char *token2 = strtok(token, ";");
                    sprintf(string, "%s/database/%s", cwd, token2);
                    if (dp != NULL) {
                        while ((ep = readdir(dp))) {

                            if (ep->d_type == DT_REG) {
                                puts(ep->d_name);
                                sprintf(fileDir, "%s/%s", string, ep->d_name);
                                remove(fileDir);
                            }
                        }
                        (void)closedir(dp);
                    }
                    int check = rmdir(string);
                    if (!check) {
                        write(fd, "Drop Database berhasil\n", SIZE_BUFFER);

                        chdir(tempDir);
                    }
                    else {
                        write(fd, "Drop Database gagal!\n", SIZE_BUFFER);
                    }
                }
            }
            else if (strcmp(cmd, "TABLE") == 0) {
                char string[5000], cwd[PATH_MAX];
                char *token = strtok(NULL, " ");

                if (getcwd(cwd, sizeof(cwd)) != NULL) {
                    char *token2 = strtok(token, ";");
                    sprintf(string, "%s/%s", cwd, token2);
                    printf("%s\n", string);

                    if (strstr(cwd, "database/") == NULL) {
                        write(fd, "Anda belum menggunakan database!\n", SIZE_BUFFER);
                    }
                    else {
                        int check = remove(string);

                        if (!check) {
                            write(fd, "Remove table success\n", SIZE_BUFFER);
                        }
                        else {
                            write(fd, "Remove table failed\n", SIZE_BUFFER);
                        }
                    }
                }
            }
            else {
                write(fd, "drop gagal\n", SIZE_BUFFER);
            }
        }
        else if (strcmp(cmd, "DELETE") == 0) {
            cmd = strtok(NULL, " ");

            if (strcmp(cmd, "FROM") == 0) {
                char string[5000], cwd[PATH_MAX];
                char *token = strtok(NULL, " ");

                if (getcwd(cwd, sizeof(cwd)) != NULL) {
                    FILE *data;
                    char firstLine[1000];

                    char *token2 = strtok(token, ";");
                    sprintf(string, "%s/%s", cwd, token2);
                    printf("%s\n", string);

                    if (strstr(cwd, "database/") == NULL) {
                        write(fd, "Anda belum menggunakan database!\n", SIZE_BUFFER);
                    }
                    else {
                        data = fopen(string, "r");

                        if (data) {
                            data = fopen(string, "w");
                            fprintf(data, "%s\n", firstLine);
                            write(fd, "Delete data success\n", SIZE_BUFFER);
                            fclose(data);
                        }
                        else {
                            write(fd, "Delete data failed\n", SIZE_BUFFER);
                        }
                    }
                }
            }
        }
        else if (strcmp(cmd, "INSERT") == 0) {
            cmd = strtok(NULL, " ");

            if (strcmp(cmd, "INFO") == 0) {
                char fileDir[5000], cwd[PATH_MAX];
                char *tableName = strtok(NULL, " ");

                printf("table: %s\n", tableName);

                if (getcwd(cwd, sizeof(cwd)) != NULL) {
                    FILE *data;
                    char *value = strtok(NULL, "()");

                    printf("value: %s\n", value);
                    sprintf(fileDir, "%s/%s", cwd, tableName);
                    printf("dir: %s\n", fileDir);

                    if (strstr(cwd, "database/") == NULL) {
                        write(fd, "Anda belum menggunakan database!\n", SIZE_BUFFER);
                    }
                    else if (value == NULL) {
                        write(fd, "Invalid syntax\n", SIZE_BUFFER);
                    }
                    else if (fileExists(fileDir)) {
                        data = fopen(fileDir, "a");
                        fprintf(data, "%s\n", value);
                        fclose(data);
                        write(fd, "Data berhasil dimasukan\n", SIZE_BUFFER);
                    }
                    else {
                        write(fd, "Tabel tidak dimasukan\n", SIZE_BUFFER);
                    }
                }
            }
        }
        else {
            write(fd, "Invalid query\n", SIZE_BUFFER);
        }
    }
    if (fd == curr_fd) {
        curr_fd = curr_id = -1;
    }
    close(fd);
}

bool login(int fd, char *username, char *password) {
    int id = -1;
    if (curr_fd != -1) {
        write(fd, "Server is busy\n", SIZE_BUFFER);
        return false;
    }

    if (strcmp(username, "root") == 0) {
        id = 0;
    }

    if (id != -1) {
        write(fd, "Login success\n", SIZE_BUFFER);
        curr_fd = fd;
        curr_id = id;
    }
    return true;
}

bool fileExists(char *fileName) {
    struct stat buffer;
    return (stat(fileName, &buffer) == 0);
}

void logging(char *str) {
    FILE *fp = fopen("/home/danielpepuho/Desktop/Github/fp-sisop-b13-2022/sys.log", "a+");
    time_t rawtime;
    struct tm *info;
    char buffer[1000];

    time (&rawtime);

    info = localtime(&rawtime);
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %X", info);
    fprintf(fp, "%s:root:%s\n", buffer, str);
    fclose(fp);
}